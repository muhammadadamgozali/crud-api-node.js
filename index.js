const express = require("express");
const app = express();
const port = 5000;
const db = require("./connection");
app.use(express.json());

app.get("/products", (req, res) => {
  const sqlQuery = "SELECT * FROM products";
  db.query(sqlQuery, (err, result) => {
    if (result.length > 0) {
      res.json({
        status: 200,
        data: result,
        message: "success all detail product",
      });
    } else {
      res.json({
        status: 500,
        data: result,
        message: "failed all detail product",
      });
    }
  });
});

app.get("/products/:id", (req, res) => {
  const id = req.params.id;
  const sqlQuery = `SELECT * from products where id = ${id}`;
  db.query(sqlQuery, (err, result) => {
    if (result.length > 0) {
      res.json({
        status: 200,
        data: result,
        message: "success get detail product by id",
      });
    } else {
      res.json({
        status: 404,
        data: result,
        message: "failed get detail product by id",
      });
    }
  });
});

app.post("/products", (req, res) => {
  const body = req.body;
  const sqlQuery = `insert into products set name = "${body.name}", stock = ${body.stock}, category = "${body.category}", description = "${body.description}", createdAt = NOW(), updatedAt = NOW()`;
  db.query(sqlQuery, (err, result) => {
    if (result) {
      res.json({
        status: 201,
        data: result,
        message: "success create detail product",
      });
    } else {
      res.json({
        status: 400,
        data: result,
        message: "failed create detail product",
      });
    }
  });
});

app.put("/products/:id", (req, res) => {
  const id = req.params.id;
  const body = req.body;
  const sqlQuery = `update products set name = "${body.name}", stock = ${body.stock}, category = "${body.category}", description = "${body.description}" where id = ${id}`;
  db.query(sqlQuery, (err, result) => {
    if (result) {
      res.json({
        status: 200,
        data: result,
        message: "success update detail product",
      });
    } else {
      res.json({
        status: 400,
        data: result,
        message: "failed update detail product",
      });
    }
  });
});

app.delete("/products/:id", (req, res) => {
  const id = req.params.id;
  const body = req.body;
  const sqlQuery = `delete from products where id = ${id}`;
  db.query(sqlQuery, (err, result) => {
    if (result) {
      res.json({
        status: 200,
        data: result,
        message: "success delete detail product",
      });
    } else {
      res.json({
        status: 400,
        data: result,
        message: "failed delete detail product",
      });
    }
  });
});

app.listen(port, () => {
  console.log(`Service api run at port ${port}`);
});
