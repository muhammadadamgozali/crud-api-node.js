# CRUD Product Express.js


## Description

CRUD Product Express.js is a Create, Read, Update, and Delete (CRUD) project using Expressjs and a RESTful API implementation that integrates with mysql databases. I added Sequelize ORM to map the database to make the development process faster.

## Clone Project
```
  git clone https://gitlab.com/muhammadadamgozali/backend-expressjs-crud-api-product.git
  cd backend-expressjs-crud-api-product
```

## Run Project
```
 npm install
 create database api_service
 npm install sequelize mysql2
 npx sequelize-cli init
 npx sequelize-cli model:generate --name Product --attributes name:string,stock:integer,category:string,description:string
 npx sequelize-cli db:migrate
 npm run start
```

## Endpoint
Follow link Postman's documentation for the collection endpoint

[Postman Documentation Product Express.js](https://documenter.getpostman.com/view/33733940/2sAYHxoPnC)


